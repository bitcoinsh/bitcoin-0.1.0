Open BSH License
Copyright (c) 2019 BIBLE.COOP

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

1 - The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

2 - The Software, and any software that is derived from the Software or parts
thereof, can only be used on the BITCOIN.FILM, BITCOIN.SH, BITCOIN.TEAM block
chains. The BITCOIN.FILM, BITCOIN.SH and BITCOIN.TEAM block chains are defined,
for purposes of this license, as software that is derived from the Bitcoin
transaction hash
"dd727e4541b812b7fb34d76db020f2440efe240beb791b3f7f146eb72341ce1c" where
the Bitcoin block chain containing the transaction hash does so at block
height #594635 with the hash
"00000000000000000197fca2cb4f3bfeaec8a362f1ddb2408237b30c5a36f13c" and the
transaction hash
"736012f6925b0375483e18733437cb187629d82d40d49ebfedb131f0ecfe5459" where
the Bitcoin block chain containing the transaction hash does so at block
height #597266 with the hash
"0000000000000000066158486aa41d344d2b64c291bd78310751e4465ddc2ef4" and the
test block chains that are supported by the un-modified Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

All prior versions of software upon which this was based, were licensed under
the MIT/X11 software license, which is included below.

The MIT/X11 License

Copyright (c) 2009 Satoshi Nakamoto
Copyright (c) 2019 Drew DeSantis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
