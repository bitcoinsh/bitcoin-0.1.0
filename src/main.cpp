#include <iostream>
//#include <headers.h>
//#include <util.h>
//#include <main.h>
//#include <serialize.h>
//#include <uint256.h>
//#include <key.h>
//#include <bignum.h>
//#include <script.h>
//#include <db.h>
//#include <base58.h>
/* main.cpp :: 0.1.0 : August 26, 2019
 *
 * Copyright (c) 2019 BIBLE.COOP
 * Copyright (c) 2019 Drew DeSantis
 * Copyright (c) 2009 Satoshi Nakamoto
 *
 * Distributed under the Open BSH software license, see the accompanying file
 * LICENSE.txt.
*/ using namespace std;

int addKey()/* 
bool AddKey(const CKey& key)*/
{
	return 0;
}

int generateNewKey()/*
vector<unsigned char> GenerateNewKey()*/
{
	return 0;
}

int addToWallet()/*
bool AddToWallet(const CWalletTx& wtxIn)*/
{
	return 0;
}

int addToWalletIfMine()/*
bool AddToWalletIfMine(const CTransaction& tx, const CBlock* pblock)*/
{
	return 0;
}

int eraseFromWallet()/*
bool EraseFromWallet(uint256 hash)*/
{
	return 0;
}

int addOrphanTx()/*
void AddOrphanTx(const CDataStream& vMsg)*/
{
	return 0;
}

int eraseOrphanTx()/*
void EraseOrphanTx(uint256 hash)*/
{
	return 0;
}

int ctxInIsMine()/*
bool CTxIn::IsMine() const*/
{
	return 0;
}

int ctxIngetDebit()/*
int64 CTxIn::GetDebit() const*/
{
	return 0;
}

int cWalletTxGetTime()/*
int64 CWalletTx::GetTxTime() const*/
{
	return 0;
}

int cMerkleTxSetMerkleBranch()/*
int CMerkleTx::SetMerkleBranch(const CBlock* pblock)*/
{
	return 0;
}

int cWalletTxAddSupportingTransactions()/*
void CWalletTx::AddSupportingTransactions(CTxDB& txdb)*/
{
	return 0;
}

int cTransactionAcceptTransaction()/*
bool CTransaction::AcceptTransaction(CTxDB& txdb, bool fCheckInputs,
                                                  bool* pfMissingInputs)*/
{
	return 0;
}

int cTransactionAddToMemoryPool()/*
bool CTransaction::AddToMemoryPool()*/
{
	return 0;
}

int cTransactionRemoveFromMemoryPool()/*
bool CTransaction::RemoveFromMemoryPool()*/
{
	return 0;
}

int cMerkleTxGetDepthInMainChain()/*
int CMerkleTx::GetDepthInMainChain() const*/
{
	return 0;
}

int cMerkleTxGetBlocksToMaturity()/*
int CMerkleTx::GetBlocksToMaturity() const*/
{
	return 0;
}

int cMerkleTxAcceptTransaction()/*
bool CMerkleTx::AcceptTransaction(CTxDB& txdb, bool fCheckInputs)*/
{
	return 0;
}

int cWalletTxAcceptWalletTransaction()/*
bool CWalletTx::AcceptWalletTransaction(CTxDB& txdb, bool fCheckInputs)*/
{
	return 0;
}

int reacceptWalletTransactions()/*
void ReacceptWalletTransactions()*/
{
	return 0;
}

int cWalletTxRelayWalletTransactions()/*
void CWalletTx::RelayWalletTransaction(CTxDB& txdb)*/
{
	return 0;
}

int relayWalletTransactions()/*
void RelayWalletTransactions()*/
{
	return 0;
}

int cBlockReadFromDisk()/*
bool CBlock::ReadFromDisk(const CBlockIndex* pblockindex, bool fReadTransactions)*/
{
	return 0;
}

int getOrphanRoot()/*
uint256 GetOrphanRoot(const CBlock* pblock)*/
{
	return 0;
}

int cBlockGetBlockValue()/*
int64 CBlock::GetBlockValue(int64 nFees) const*/
{
	return 0;
}

int getNextWorkRequired()/*
unsigned int GetNextWorkRequired(const CBlockIndex* pindexLast)*/
{
	return 0;
}

int cTransactionDisconnectInputs()/*
bool CTransaction::DisconnectInputs(CTxDB& txdb)*/
{
	return 0;
}

int cTransactionConnectInputs()/*
bool CTransaction::ConnectInputs(CTxDB& txdb, map<uint256, CTxIndex>& mapTestPool, CDiskTxPos posThisTx, int nHeight, int64& nFees, bool fBlock, bool fMiner, int64 nMinFee)*/
{
	return 0;
}

int cTransactionClientConnectInputs()/*
bool CTransaction::ClientConnectInputs()*/
{
	return 0;
}

int cBlockDisconnectBlock()/*
bool CBlock::DisconnectBlock(CTxDB& txdb, CBlockIndex* pindex)*/
{
	return 0;
}

int cBlockConnectBlock()/*
bool CBlock::ConnectBlock(CTxDB& txdb, CBlockIndex* pindex)*/
{
	return 0;
}

int reorganize()/*
bool Reorganize(CTxDB& txdb, CBlockIndex* pindexNew)*/
{
	return 0;
}

int cBlockAddToBlockIndex()/*
bool CBlock::AddToBlockIndex(unsigned int nFile, unsigned int nBlockPos)*/
{
	return 0;
}

int cBlockCheckBlock()/*
bool CBlock::CheckBlock() const*/
{
	return 0;
}

int cBlockAcceptBlock()/*
bool CBlock::AcceptBlock()*/
{
	return 0;
}

int processBlock()/*
bool ProcessBlock(CNode* pfrom, CBlock* pblock)*/
{
	return 0;
}

int scanMessageStart()/*
template<typename Stream>
bool ScanMessageStart(Stream& s)*/
{
	return 0;
}

int getAppDir()/*
string GetAppDir()*/
{
	return 0;
}

int openBlockFile()/*
FILE* OpenBlockFile(unsigned int nFile, unsigned int nBlockPos, const char* pszMode)*/
{
	return 0;
}

int appendBlockFile()/*
FILE* AppendBlockFile(unsigned int& nFileRet)*/
{
	return 0;
}

int loadBlockIndex()/*
bool LoadBlockIndex(bool fAllowNew)*/
{
	return 0;
}

int printBlockTree()/*
void PrintBlockTree()*/
{
	return 0;
}

int alreadyHave()/*
bool AlreadyHave(CTxDB& txdb, const CInv& inv)*/
{
	return 0;
}

int processMessages()/*
bool ProcessMessages(CNode* pfrom)*/
{
	return 0;
}

int processMessage()/*
bool ProcessMessage(CNode* pfrom, string strCommand, CDataStream& vRecv)*/
{
	return 0;
}

int sendMessages()/*
bool SendMessages(CNode* pto)*/
{
	return 0;
}

int formatHashBlocks()/*
int FormatHashBlocks(void* pbuffer, unsigned int len)*/
{
	return 0;
}

int bitcoinMiner()/*
bool BitcoinMiner()*/
{
	return 0;
}

int getBalance()/*
int64 GetBalance()*/
{
	return 0;
}

int selectCoins()/*
bool SelectCoins(int64 nTargetValue, set<CWalletTx*>& setCoinsRet)*/
{
	return 0;
}

int createTransaction()/*
bool CreateTransaction(CScript scriptPubKey, int64 nValue, CWalletTx& wtxNew, int64& nFeeRequiredRet)*/
{
	return 0;
}

int commitTransactionSpent()/*
bool CommitTransactionSpent(const CWalletTx& wtxNew)*/
{
	return 0;
}

int sendMoney()/*
bool SendMoney(CScript scriptPubKey, int64 nValue, CWalletTx& wtxNew)*/
{
	return 0;
}

int main()
{	
	int test_addKey                             = addKey();
	int test_generateNewKey                     = generateNewKey();
	int test_addToWallet                        = addToWallet();
	int test_addToWalletIfMine                  = addToWalletIfMine();
	int test_eraseFromWallet                    = eraseFromWallet();
	int test_addOrphanTx                        = addOrphanTx();
	int test_eraseOrphanTx                      = eraseOrphanTx();
	int test_ctxInIsMine                        = ctxInIsMine();
	int test_ctxIngetDebit                      = ctxIngetDebit();
	int test_cWalletTxGetTime                   = cWalletTxGetTime();
	int test_cMerkleTxSetMerkleBranch           = cMerkleTxSetMerkleBranch();
	int test_cWalletTxAddSupportingTransactions = cWalletTxAddSupportingTransactions();
	int test_cTransactionAcceptTransaction      = cTransactionAcceptTransaction();
	int test_cTransactionAddToMemoryPool        = cTransactionAddToMemoryPool();
	int test_cTransactionRemoveFromMemoryPool   = cTransactionRemoveFromMemoryPool();
	int test_cMerkleTxGetDepthInMainChain       = cMerkleTxGetDepthInMainChain();
	int test_cMerkleTxGetBlocksToMaturity       = cMerkleTxGetBlocksToMaturity();
	int test_cMerkleTxAcceptTransaction         = cMerkleTxAcceptTransaction();
	int test_cWalletTxAcceptWalletTransaction   = cWalletTxAcceptWalletTransaction();
	int test_reacceptWalletTransactions         = reacceptWalletTransactions();
	int test_cWalletTxRelayWalletTransactions   = cWalletTxRelayWalletTransactions();
	int test_relayWalletTransactions            = relayWalletTransactions();
	int test_cBlockReadFromDisk                 = cBlockReadFromDisk();
	int test_getOrphanRoot                      = getOrphanRoot();
	int test_cBlockGetBlockValue                = cBlockGetBlockValue();
	int test_getNextWorkRequired                = getNextWorkRequired();
	int test_cTransactionDisconnectInputs       = cTransactionDisconnectInputs();
	int test_cTransactionConnectInputs          = cTransactionConnectInputs();
	int test_cTransactionClientConnectInputs    = cTransactionClientConnectInputs();
	int test_cBlockDisconnectBlock              = cBlockDisconnectBlock();
	int test_cBlockConnectBlock                 = cBlockConnectBlock();
	int test_reorganize                         = reorganize();
	int test_cBlockAddToBlockIndex              = cBlockAddToBlockIndex();
	int test_cBlockCheckBlock                   = cBlockCheckBlock();
	int test_cBlockAcceptBlock                  = cBlockAcceptBlock();
	int test_processBlock                       = processBlock();
	int test_scanMessageStart                   = scanMessageStart();
	int test_getAppDir                          = getAppDir();
	int test_openBlockFile                      = openBlockFile();
	int test_appendBlockFile                    = appendBlockFile();
	int test_loadBlockIndex                     = loadBlockIndex();
	int test_printBlockTree                     = printBlockTree();
	int test_alreadyHave                        = alreadyHave();
	int test_processMessages                    = processMessages();
	int test_sendMessages                       = sendMessages();
	int test_formatHashBlocks                   = formatHashBlocks();
	int test_bitcoinMiner                       = bitcoinMiner();
	int test_getBalance                         = getBalance();
	int test_selectCoins                        = selectCoins();
	int test_createTransaction                  = createTransaction();
	int test_commitTransactionSpent             = commitTransactionSpent();
	int test_sendMoney                          = sendMoney();

	cout << "\n";
	cout << "addKey"                             << " : " << test_addKey                             << "\n";
	cout << "generateNewKey"                     << " : " << test_generateNewKey                     << "\n";
	cout << "addToWallet"                        << " : " << test_addToWallet                        << "\n";
	cout << "addToWalletIfMine"                  << " : " << test_addToWalletIfMine                  << "\n";
	cout << "eraseFromWallet"                    << " : " << test_eraseFromWallet                    << "\n";
	cout << "addOrphanTx"                        << " : " << test_addOrphanTx                        << "\n";
	cout << "eraseOrphanTx"                      << " : " << test_eraseOrphanTx                      << "\n";
	cout << "ctxInIsMine"                        << " : " << test_ctxInIsMine                        << "\n";
	cout << "ctxIngetDebit"                      << " : " << test_ctxIngetDebit                      << "\n";
	cout << "cWalletTxGetTime"                   << " : " << test_cWalletTxGetTime                   << "\n";
	cout << "cMerkleTxSetMerkleBranch"           << " : " << test_cMerkleTxSetMerkleBranch           << "\n";
	cout << "cWalletTxAddSupportingTransactions" << " : " << test_cWalletTxAddSupportingTransactions << "\n";
	cout << "cTransactionAcceptTransaction"      << " : " << test_cTransactionAcceptTransaction      << "\n";
	cout << "cTransactionAddToMemoryPool"        << " : " << test_cTransactionAddToMemoryPool        << "\n";
	cout << "cTransactionRemoveFromMemoryPool"   << " : " << test_cTransactionRemoveFromMemoryPool   << "\n";
	cout << "cMerkleTxGetDepthInMainChain"       << " : " << test_cMerkleTxGetDepthInMainChain       << "\n";
	cout << "cMerkleTxGetBlocksToMaturity"       << " : " << test_cMerkleTxGetBlocksToMaturity       << "\n";
	cout << "cMerkleTxAcceptTransaction"         << " : " << test_cMerkleTxAcceptTransaction         << "\n";
	cout << "cWalletTxAcceptWalletTransaction"   << " : " << test_cWalletTxAcceptWalletTransaction   << "\n";
	cout << "reacceptWalletTransactions"         << " : " << test_reacceptWalletTransactions         << "\n";
	cout << "cWalletTxRelayWalletTransactions"   << " : " << test_cWalletTxRelayWalletTransactions   << "\n";
	cout << "relayWalletTransactions"            << " : " << test_relayWalletTransactions            << "\n";
	cout << "cBlockReadFromDisk"                 << " : " << test_cBlockReadFromDisk                 << "\n";
	cout << "getOrphanRoot"                      << " : " << test_getOrphanRoot                      << "\n";
	cout << "cBlockGetBlockValue"                << " : " << test_cBlockGetBlockValue                << "\n";
	cout << "getNextWorkRequired"                << " : " << test_getNextWorkRequired                << "\n";
	cout << "cTransactionDisconnectInputs"       << " : " << test_cTransactionDisconnectInputs       << "\n";
	cout << "cTransactionConnectInputs"          << " : " << test_cTransactionConnectInputs          << "\n";
	cout << "cTransactionClientConnectInputs"    << " : " << test_cTransactionClientConnectInputs    << "\n";
	cout << "cBlockDisconnectBlock"              << " : " << test_cBlockDisconnectBlock              << "\n";
	cout << "cBlockConnectBlock"                 << " : " << test_cBlockConnectBlock                 << "\n";
	cout << "reorganize"                         << " : " << test_reorganize                         << "\n";
	cout << "cBlockAddToBlockIndex"              << " : " << test_cBlockAddToBlockIndex              << "\n";
	cout << "cBlockCheckBlock"                   << " : " << test_cBlockCheckBlock                   << "\n";
	cout << "cBlockAcceptBlock"                  << " : " << test_cBlockAcceptBlock                  << "\n";
	cout << "processBlock"                       << " : " << test_processBlock                       << "\n";
	cout << "scanMessageStart"                   << " : " << test_scanMessageStart                   << "\n";
	cout << "getAppDir"                          << " : " << test_getAppDir                          << "\n";
	cout << "openBlockFile"                      << " : " << test_openBlockFile                      << "\n";
	cout << "appendBlockFile"                    << " : " << test_appendBlockFile                    << "\n";
	cout << "loadBlockIndex"                     << " : " << test_loadBlockIndex                     << "\n";
	cout << "printBlockTree"                     << " : " << test_printBlockTree                     << "\n";
	cout << "alreadyHave"                        << " : " << test_alreadyHave                        << "\n";
	cout << "processMessages"                    << " : " << test_processMessages                    << "\n";
	cout << "sendMessages"                       << " : " << test_sendMessages                       << "\n";
	cout << "formatHashBlocks"                   << " : " << test_formatHashBlocks                   << "\n";
	cout << "bitcoinMiner"                       << " : " << test_bitcoinMiner                       << "\n";
	cout << "getBalance"                         << " : " << test_getBalance                         << "\n";
	cout << "selectCoins"                        << " : " << test_selectCoins                        << "\n";
	cout << "createTransaction"                  << " : " << test_createTransaction                  << "\n";
	cout << "commitTransactionSpent"             << " : " << test_commitTransactionSpent             << "\n";
	cout << "sendMoney"                          << " : " << test_sendMoney                          << "\n";

	return 0;
}
