#include <nroot.h>

/* #!de : main.c :: 0.1.0 : August 26, 2019
 *
 * Copyright (c) 2019 BIBLE.COOP
 * Copyright (c) 2019 Drew DeSantis
 * Copyright (c) 2009 Satoshi Nakamoto
 *
 * Distributed under the Open BSH software license, see the accompanying file
 * LICENSE.txt.
*/

int
main(int argc, char const *argv[])
{
	printf("Hello, world!\n");
	return 0;
}
