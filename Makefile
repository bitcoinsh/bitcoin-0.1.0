#!de :: bitcoinsh/bitcoin-0.1.0 : Makefile

# Copyright (c) 2019 BIBLE.COOP
# Copyright (c) 2019 Drew DeSantis
# Copyright (c) 2009 Satoshi Nakamoto

# Distributed under the Open BSH software license, see the accompanying file
# LICENSE.txt.

include .deosrc

all: run
	@echo $(call l,\n${cBl},${cGr}[${cPu}4${cGr}] ${cCy}$@)

run: build
	@echo $(call l,\n${cBl},${cGr}[${cPu}3${cGr}] ${cCy}$@)
	$(EXE)

build: clean
	@echo $(call l,\n${cBl},${cGr}[${cPu}2${cGr}] ${cCy}$@)
	$(CC) $(MAIN_C) -o $(EXE)

clean:
	@echo $(call l,\n${cBl},${cGr}[${cPu}1${cGr}] ${cCy}$@)
	@-rm -rf $(EXE)
